package com.egs.maven.util;

import com.egs.maven.model.User;

public class VerificationMessage {

    public static String getVerificationMessage(User user){
        return "Hello dear" + user.getName().substring(0,1).toUpperCase()
                + user.getName().substring(1) +
                " You were successfully registered . Please verify . Your verification code is : " +
                user.getCode();
    }

}
