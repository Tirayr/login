package com.egs.maven.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcUtil {

    private static String url = "jdbc:mysql://localhost:3306/spr";
    private static String driver = "com.mysql.jdbc.Driver";
    private static String username = "root";
    private static String password = "mmtiro";

    private static Connection connection;

    public static Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {
            try{
                Class.forName(driver);
                connection = DriverManager.getConnection(url , username, password);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }
}
