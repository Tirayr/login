package com.egs.maven.util;

import java.util.Random;

public class GenerateNumber {

    private static final char[] NUMBERS =  "0123456789".toCharArray();

    public static String getGenerateCode(){
        int count = 7;
        StringBuilder sb = new StringBuilder();
        Random random = new Random();

        for (int i = 0; i < count; i++) {
            char c = NUMBERS[random.nextInt(NUMBERS.length)];
            sb.append(c);
        }
        return sb.toString();
    }

}
