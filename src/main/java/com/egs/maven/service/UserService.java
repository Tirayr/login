package com.egs.maven.service;

import com.egs.maven.model.User;
import com.egs.maven.repository.UserRepository;


public class UserService {

    private UserRepository userRepository = new UserRepository();
    public void add(User user) {
        userRepository.add(user);
    }

    public User getUser(String email) {
      User user = userRepository.getByEmail(email);
        return user;
    }

    public void update(User user) {
        userRepository.update(user);
    }

    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }
}
