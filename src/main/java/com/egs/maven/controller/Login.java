package com.egs.maven.controller;

import com.egs.maven.model.User;
import com.egs.maven.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

public class Login extends HttpServlet {

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email;
        String password;

        if (request.getAttribute("email") != null) {
            email = (String) request.getAttribute("email");
            password = (String) request.getAttribute("password");
        } else {
            email = request.getParameter("email");
            password = request.getParameter("password");
        }

        if (validate(email, password) != null) {
            request.setAttribute("message", validate(email, password));
            request.getRequestDispatcher("/index.jso").forward(request, response);
            return;
        }

        UserService userService = new UserService();
        User user = userService.getByEmail(email);

        if (user == null || !user.getPassword().equals(password)){
            request.setAttribute("Message","wrong email or password");
            request.getRequestDispatcher("/index.jsp").forward(request,response);
            return;
        }

        HttpSession session = request.getSession();
        session.setAttribute("user",user);

        if (!user.isVerify()){
            request.setAttribute("Message","please");
            request.getRequestDispatcher("/pages/verifivation.jsp").forward(request,response);
            return;
        }

        checkCookie(request,response);

        response.sendRedirect("/pages/home.jsp");



    }


    public String validate(String... params) {
        for (String s : params) {
            if (params == null || params.equals("")) {
                return "All params are required";
            }
        }
        return null;
    }

    public void checkCookie(HttpServletRequest request, HttpServletResponse response){
        String remember = request.getParameter("remember");

        if (remember != null){
            Cookie cookie1 = new Cookie("email",request.getParameter("email"));
            Cookie cookie2 = new Cookie("password",request.getParameter("password"));
            cookie1.setMaxAge(100000);
            cookie2.setMaxAge(100000);
            response.addCookie(cookie1);
            response.addCookie(cookie2);
        }
    }
}
