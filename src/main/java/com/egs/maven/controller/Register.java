package com.egs.maven.controller;

import com.egs.maven.model.User;
import com.egs.maven.service.UserService;
import com.egs.maven.util.GenerateNumber;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class Register extends HttpServlet {

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        String name = req.getParameter("name");
        int age = Integer.parseInt(req.getParameter("age"));
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        UserService userService = new UserService();

        if (validate(name, Integer.toString(age), email, password) != null) {
            req.setAttribute("Message", validate(name, Integer.toString(age), email, password));
            req.getRequestDispatcher("/index.jsp").forward(req, res);
            return;
        }

        User user = new User();
        user.setName(name);
        user.setAge(age);
        user.setEmail(email);
        user.setPassword(password);
        user.setVerify(false);
        user.setCode(GenerateNumber.getGenerateCode());

        userService.add(user);
        req.getSession().setAttribute("user", user);


        res.sendRedirect("/pages/verification.jsp");

    }

    public String validate(String... params) {
        for (String param : params) {
            if (param == null || param.equals("")) {
                return "All params are required";
            }
        }
        return null;
    }

}
