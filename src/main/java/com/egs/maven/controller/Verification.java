package com.egs.maven.controller;


import com.egs.maven.model.User;
import com.egs.maven.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Verification extends HttpServlet {
    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");

        String code = req.getParameter("code");
        if (!code.equals(user.getCode())){
            req.setAttribute("Message", "Wrong verification code");
            req.getRequestDispatcher("/page/verification.jsp").forward(req,res);
            return ;
        }

        user.setVerify(true);
        user.setCode(null);
        UserService userService = new UserService();
        userService.update(user);
        req.setAttribute("Message","You have successfully verified");
        req.getRequestDispatcher("/pages/home.jsp").forward(req,res);

    }
}
