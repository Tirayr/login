package com.egs.maven.controller;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Start extends HttpServlet {


    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = null;
        String password = null;

        Cookie[] cookies = request.getCookies();
        if (cookies == null){
            response.sendRedirect("/index.jsp");
            return;
        }

        for (Cookie cookie : cookies){
            if (cookie.getName().equals("email")){
                email = cookie.getValue();
            }
            if (cookie.getName().equals("password")){
                password = cookie.getValue();
            }
        }

        Login login = new Login();
        request.setAttribute("email",email);
        request.setAttribute("password",password);

        login.service(request,response);

    }
}
