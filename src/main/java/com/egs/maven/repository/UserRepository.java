package com.egs.maven.repository;

import com.egs.maven.model.User;
import com.egs.maven.util.JdbcUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRepository {
    public void add(User user) {
        try {
            Connection connection = JdbcUtil.getConnection();
            PreparedStatement prstm = connection.prepareStatement("insert into spr.users values (0,?,?,?,?,?,?);");
            prstm.setString(1,user.getName());
            prstm.setInt(2,user.getAge());
            prstm.setBoolean(3,user.isVerify());
            prstm.setString(4,user.getCode());
            prstm.setString(5,user.getEmail());
            prstm.setString(6,user.getPassword());


            prstm.execute();
            prstm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public User getByEmail(String email) {
        User user = null;
        try(Connection connection = JdbcUtil.getConnection()) {
            PreparedStatement prstm = connection.prepareStatement("select * from spr.users where email = ?;");
            prstm.setString(1,email);
            ResultSet rs = prstm.executeQuery();

            if (rs.next()){
                user = new User();
                user.setId(rs.getInt("id"));
                user.setName(rs.getString("name"));
                user.setAge(rs.getInt("age"));
                user.setVerify(rs.getBoolean("verify"));
                user.setCode(rs.getString("code"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("password"));


            }
            rs.close();
            prstm.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;

    }

    public void update(User user) {
        try {
            Connection connection = JdbcUtil.getConnection();
            PreparedStatement prstm = connection.prepareStatement("update spr.users set name = ?,age = ?, " +
                    "verify = ?, code = ?, email = ?, password = ?,  where  id = ?");
            prstm.setString(1,user.getName());
            prstm.setInt(2,user.getAge());
            prstm.setBoolean(6,user.isVerify());
            prstm.setString(5,user.getCode());
            prstm.setString(3,user.getEmail());
            prstm.setString(4,user.getPassword());
            prstm.setInt(7,user.getId());

            prstm.executeUpdate();
            prstm.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
