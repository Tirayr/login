<%--
  Created by IntelliJ IDEA.
  User: SaqoTiro
  Date: 4/25/2019
  Time: 6:39 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Log in</title>
</head>
<body>
<h1> LOG IN </h1>

<%
    String error = (String) request.getAttribute("Message");
    if (error != null && !error.equals("")) {
%>
<h4 style="color: #26ffb2"><i><%=error%>
</i></h4>
<%}%>


<form action="/login" method="post">
    Email: <input type="text" name="email"/> <br>
    <br>
    Password: <input type="password" name="password"/> <br>
    <br>
    rememberMe <input type="checkbox" name="remember"><br>
    <br>
    <input type="submit" name="submit" value="log in">
</form>

<a href="/pages/register.jsp">Go To Registration Page</a>
</body>
</html>
